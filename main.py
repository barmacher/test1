# Создайте пример использования циклов с массивом данных состоящих из цифр
a = [1, 2, 3, 4, 5, 6, 7, 8, 9]
for b in a:
    print(b)
def razdelenie():
    print('---------------')
razdelenie()
#Создайте пример использования циклов с массивом данных состоящих из словарей (dict)
dict = {
    'Igor': 4,
    "Malik": 5,
    "Eldar": 5,
    "Nuraly": 6

}
for l in dict:
    print(l)
    razdelenie()
#Создайте простую функцию - выводящую в  консоль слово 'Horosho'
def horosho():
    print('Horosho')
horosho()
razdelenie()
#Создайте простую функцию - складывающую 5 + 5
def slojenie():
    print(5+5)
slojenie()
razdelenie()
#Создайте простую функцию - которая принимает в себя число и отнимает от принятого числа 5
def t(chislo):
    print(chislo - 5)
t(32112)
razdelenie()
#Создайте простую функцию - которая принимает в себя число и умножает его на 5
def p(chislo2):
    print(chislo2 * 5)
p(20)
razdelenie()
#Создайте простую функцию - которая принимает в себя число и вычисляет 15 процентов от принятого числа
def proc(chislo3):
    print(chislo3 * 0.15)
proc(1000)
razdelenie()
#Создайте простую функцию - которая принимает в себя строку и добавляет ей в конец слово 'hello'
def slovoplus(slovo):
    print(slovo + ' hello')
slovoplus('privet')
razdelenie()
#Создать свой клуб с использованием ммассива людей, и функции охранника для фильтрации людей
moi_drugi =[
    {
    'imya': 'Eldar',
    'vozrast': 19,
    'volosi': 'brunet',
    'bolezn': '-'
},
{
    'imya': 'Segei',
    'vozrast': 15,
    'volosi': 'blondin',
    'bolezn': '-'
},
{
    'imya': 'Eleonora',
    'vozrast': 20,
    'volosi': 'brunetka',
    'bolezn': '-'
},
{
    'imya': 'Karina',
    'vozrast': 17,
    'volosi': 'blondinka',
    'bolezn': '+'
},
{
     'imya': 'Vera',
    'vozrast': 23,
    'volosi': 'blondinka',
    'bolezn': '-'
}
]
for drugi in moi_drugi:
    if drugi['vozrast'] >= 18 and drugi['bolezn'] == '-':
        print(drugi['imya'], 'prohodish')
    else:
        print(drugi['imya'],'domoi')
razdelenie()
